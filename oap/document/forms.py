#-*-coding:utf-8-*-
from django import forms
from django.core.files.uploadedfile import UploadedFile
from .models import *

class DocumentForm( forms.Form ):
	document = forms.FileField()

	def clean_document(self, *args, **kwargs):
		doc = self.cleaned_data['document']
		white_list = [ 'application/vnd.oasis.opendocument.text',
					 	'application/pdf',
					 	'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
					 ]	
		if not doc.content_type in white_list:
			raise forms.ValidationError('Tipo de arquivo inválido.<br> extensões aceitas .odt, .docx.')
		return doc	

	def save(self, *args, **kwargs):
		wrapped_file = UploadedFile( self.cleaned_data['document'] )
		path = '/tmp/%s' % ( wrapped_file.name )
		destination = open( path, 'wb+')
		for chunk in wrapped_file.chunks():
			destination.write(chunk)
		destination.close()
		return path

class SettingsTextPreProcessorForm( forms.ModelForm ):
	class Meta:
		model = TextProcessorSettings 

class EditSettingsTextPreProcessorForm( forms.Form ):
	
	language = forms.ModelChoiceField(queryset=TextProcessorSettings.objects.all())
	stop_words = forms.CharField(widget=forms.Textarea)
	special_chars = forms.CharField(widget=forms.Textarea)

	def save(self, instance, commit=False):
		instance.stop_words = self.cleaned_data['stop_words']
		instance.special_chars = self.cleaned_data['special_chars']
		instance.save()
		return instance