#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response, HttpResponse, get_object_or_404, Http404
from .models import *
from .forms import *

from django.views.decorators.csrf import csrf_exempt 
from django.utils import simplejson 

@csrf_exempt 
def settings_pre_processor( request ):
	if request.method == 'POST':
		processor_form = SettingsTextPreProcessorForm( request.POST )
		result = []
		if processor_form.is_valid():
			processor_form.save()
			result.append(
							{
								"status" : 'ok', 
							}
						)
		else:

			result.append(
							{
								"status" : 'error', 
								"errors" : processor_form.errors, 
							}
						)
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()

@csrf_exempt 
def settings_edit_pre_processor( request ):
	if request.method == 'POST':
		processor_form = EditSettingsTextPreProcessorForm( request.POST )
		result = []
		if processor_form.is_valid():
			processor_form.save( processor_form.cleaned_data['language'] )
			result.append(
							{
								"status" : 'ok', 
							}
						)
		else:

			result.append(
							{
								"status" : 'error', 
								"errors" : processor_form.errors, 
							}
						)
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()

@csrf_exempt 
def get_pre_processor_settings( request ):
	if request.method == 'POST':
		result = []
		try:
			setting = get_object_or_404( TextProcessorSettings, pk=request.POST.get('language') )
			result.append(
								{
									"status" : 'ok',
									'stop_words': setting.stop_words, 
									'special_chars': setting.special_chars, 
								}
							)
		except:
			result.append(
								{
									"status" : 'error', 
								}
							)

		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()