#-*- coding: utf-8 -*-
from django.db import models
from pdfminer.pdfparser import PDFDocument, PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter, process_pdf
from pdfminer.pdfdevice import PDFDevice, TagExtractor
from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
from pdfminer.cmapdb import CMapDB
from pdfminer.layout import LAParams
from cStringIO import StringIO
from nltk import word_tokenize
from nltk.tokenize import blankline_tokenize
from xml.dom.minidom import parseString
import zipfile, re, magic
from oap.settings import *
import pyPdf
import subprocess

class DocumentConverter(object):

	@staticmethod
	def doc_to_text( doc ):
		mimetype = magic.from_file( doc.name, mime=True )
		if mimetype == 'application/pdf':
			return DocumentConverter.__pdf_to_text( doc )
		elif mimetype == 'application/vnd.oasis.opendocument.text':
			return DocumentConverter.__odt_to_text( doc )
		elif mimetype == 'application/zip':
			if DocumentConverter.__isValidDocx( doc ):
				return DocumentConverter.__docx_to_text( doc )
			else:	
				raise Exception('Invalid file type.')
		else:
			raise Exception('Invalid file type.')
		
	@staticmethod
	def __pdf_to_text( doc ):

		resource_manager = PDFResourceManager()
		retstr = StringIO()
		codec = 'utf-8'
		laparams = LAParams()
		device = TextConverter( resource_manager, retstr, codec=codec, laparams=laparams)
		process_pdf(resource_manager, device, doc)
		doc.close()
		device.close()
		doc_as_text = retstr.getvalue()
		retstr.close()
		return u"%s" % ( doc_as_text.decode('utf-8', 'ignore') )

	@staticmethod
	def __odt_to_text( doc ):
		odtfile = zipfile.ZipFile( doc )
		doc_as_xml = parseString( odtfile.read('content.xml')  )
		paragraphs = doc_as_xml.getElementsByTagName('text:p')
		doc_as_text = ''
		for paragraph in paragraphs:
			for node in paragraph.childNodes:
		
				if node.nodeType == node.TEXT_NODE:
					doc_as_text = u"%s %s\n\t" % ( doc_as_text, node.data )
				elif node.nodeType == node.ELEMENT_NODE:
					for child in node.childNodes:
						if child.nodeType == node.TEXT_NODE:
							doc_as_text =   u"%s %s\n\t" % ( doc_as_text, child.data )
		return doc_as_text

	@staticmethod
	def __docx_to_text( doc ):
		doc_as_text = ''
		docxfile = zipfile.ZipFile( doc )
		doc_as_xml = parseString( docxfile.read('word/document.xml')  )
		paragraphs = doc_as_xml.getElementsByTagName('w:t')
		doc_as_text = ''
		for paragraph in paragraphs:
			for node in paragraph.childNodes:
		
				if node.nodeType == node.TEXT_NODE:
					doc_as_text = u"%s %s\n\t" % ( doc_as_text, node.data )
				elif node.nodeType == node.ELEMENT_NODE:
					for child in node.childNodes:
						if child.nodeType == node.TEXT_NODE:
							doc_as_text =   u"%s %s\n\t" % ( doc_as_text, child.data )
		return doc_as_text

	@staticmethod
	def __isValidDocx( doc ):
		docx = zipfile.ZipFile( doc, "r")
		isValid = DocumentConverter.__isdir(docx, "word") and DocumentConverter.__isdir(docx, "docProps") and DocumentConverter.__isdir(docx, "_rels")
		docx.close()
		return isValid

	@staticmethod
	def __isdir( zipfile, directory ):
		return any( zipdir.startswith("%s/" % directory.rstrip("/")) for zipdir in zipfile.namelist())

class TextProcessorManager(models.Manager):

	def getSettings(self):
		text_processor = self.filter( language=LANGUAGE_CODE )[:1].get()
		return text_processor

class TextProcessorSettings(models.Model):
	language = models.CharField(max_length=6)
	stop_words = models.TextField()
	special_chars = models.TextField()

	objects = TextProcessorManager()

	def __unicode__(self):
		return self.language

class TextProcessor( object ):
	
	@staticmethod
	def remove_stop_words( token ):
		settings = TextProcessorSettings.objects.getSettings()
		stop_words = settings.stop_words
		stop_words = stop_words.split(' ')
		for word in stop_words:     
			token = re.sub( r'\b%s\b' % ( word ), ' ', token )
		return token	

	@staticmethod
	def remove_special_chars( token ):
		settings = TextProcessorSettings.objects.getSettings()
		special_chars = settings.special_chars
		special_chars = special_chars.split(' ')
		for word in special_chars:     
			token = token.replace( word , ' ' )
		return token


class DocumentManager(models.Manager):
	def create_doc(self, text):
		doc = self.create(text=text)
		doc.save()
		return doc
		
class Document( models.Model ):
	text = models.TextField()

	def similarity( self ):
		similarity_paragraphs = 0
		similarity = 0
		len_paragraphs = self.paragraphs.count()
		for paragraph in self.paragraphs.all():
			similarity_paragraphs += paragraph.similarity()
		
		if similarity_paragraphs > 0:
			similarity = similarity_paragraphs/len_paragraphs

		return similarity

	def tokenizer_paragraphs( self ):
		paragraphs = self.text.split( '\n\t' ) 
		for paragraph in paragraphs:
			if len( paragraph.split() ) > 20 :
				new_paragraph = Paragraph()
				new_paragraph.text = paragraph
				new_paragraph.document = self
				new_paragraph.save()
				new_paragraph.tokenizer_sentences()

	def similarity_level( self ):
		levels = ['low', 'medium', 'high']

		if self.similarity() <= 40:
			return levels[0]
		elif self.similarity() > 40 and self.similarity() <= 70:
			return levels[1]
		else:
			return levels[2]

	def __unicode__(self):
		return u"%s" % ( self.text )

	objects = DocumentManager()

class Paragraph( models.Model ):
	document = models.ForeignKey( Document, related_name="paragraphs" )
	text = models.TextField()

	EXTRACT_STATUS = (
		('COMPLETE', 'complete'),
		('PENDING', 'pending'),
	)
	extract_status = models.CharField(max_length=8, choices=EXTRACT_STATUS, default="PENDING")

	def similarity( self ):
		similarity_sentences = 0
		similarity = 0
		len_filtered_results = 0 
		for sentence in self.sentences.all():
			len_filtered_results += sentence.similarTokens.count()
			for token in sentence.similarTokens.all():
				similarity_sentences += token.similarity
		
		if similarity_sentences > 0:
			similarity = similarity_sentences/len_filtered_results
		return similarity

	def tokenizer_sentences( self ):
		text = self.text
		text = TextProcessor.remove_stop_words( text )
		text = TextProcessor.remove_special_chars( text )
		words = word_tokenize( text )
		sentence = Sentence()
		
		for word in words:
			len_new_sentence = len( sentence.text ) + len( word )
			if len_new_sentence >= 50 and len_new_sentence <= 100:
				sentence.paragraph = self
				sentence.save()
				sentence = Sentence()
			sentence.text = u"%s %s" % ( sentence.text, word )

	def similarity_level( self ):
		levels = ['low', 'medium', 'high']

		if self.similarity() <= 40:
			return levels[0]
		elif self.similarity() > 40 and self.similarity() <= 70:
			return levels[1]
		else:
			return levels[2]		

	def __unicode__(self):
		return u'%s' % ( self.text )

class Sentence( models.Model ):
	text = models.CharField( max_length=50 )
	paragraph = models.ForeignKey( Paragraph, related_name="sentences" )
	SEARCH_STATUS = (
		('COMPLETE', 'complete'),
		('PENDING', 'pending'),
	)
	search_status = models.CharField(max_length=8, choices=SEARCH_STATUS, default="PENDING")

	def __unicode__(self):
		return "%s" % ( self.text )

class SimilarToken( models.Model ):
	url = models.URLField(max_length=1000)
	text = models.TextField()
	similarity = models.IntegerField( blank=True, null=True )
	sentence = models.ForeignKey( Sentence, related_name="similarTokens" )

	def similarity_level( self ):
		levels = ['low', 'medium', 'high']

		if self.similarity <= 40:
			return levels[0]
		elif self.similarity > 40 and self.similarity <= 70:
			return levels[1]
		else:
			return levels[2]

	def highlighted_token( self ):
		token = self.text
		token = TextProcessor.remove_stop_words( token )
		token = TextProcessor.remove_special_chars( token )
		sentence_set = set( self.sentence.text.split() )
		similar_set = set( token.split() )
		equal_words = similar_set & sentence_set
		text = self.text
		for word in equal_words:
			new_word = "<b>%s</b>" % ( word )
			text = text.replace( word, new_word )	

		return text
	
	
	

