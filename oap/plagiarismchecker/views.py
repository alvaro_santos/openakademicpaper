#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response, HttpResponse, get_object_or_404, Http404
from .models import *
from .forms import *

from django.views.decorators.csrf import csrf_exempt 
from django.utils import simplejson 

@csrf_exempt 
def settings_dip( request ):
	if request.method == 'POST':
		
		dip_form = SettingsDipForm( request.POST )
		result = []
		if dip_form.is_valid():
			request.session['plagiarism_index'] = dip_form.cleaned_data['plagiarism_index']
			result.append(
							{
								"status" : 'ok', 
							}
						)
		else:

			result.append(
							{
								"status" : 'error', 
								"errors" : dip_form.errors, 
							}
						)
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()

@csrf_exempt 
def settings_default_dip( request ):
	if request.method == 'POST':
		if request.session.get('plagiarism_index') is not None:
			del request.session['plagiarism_index']
		result = []
		result.append(
						{
							"status" : 'ok',
						}
					)
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()