#-*-coding:utf-8-*-
from django import forms
from .models import *

class SettingsDipForm( forms.Form ):
	plagiarism_index = forms.IntegerField()

	def clean_plagiarism_index(self, *args, **kwargs):
		index = self.cleaned_data['plagiarism_index']
		if index < 60 or index > 100:
			raise forms.ValidationError('O índice deve estar entre 60% e 100%.')
		return index	