#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns('',
    url(r'^settings-dip$', settings_dip, name='settingsdip'),
    url(r'^settings-default-dip$', settings_default_dip, name='settingsdefaultdip'),
)