#-*-coding:utf-8-*-
from django.db import models
from oap.searcher.models import *
from oap.document.models import *
from django.conf import settings
from django.contrib.sessions.backends.db import SessionStore

# Create your models here.
class DIPSettings( object ):
	@staticmethod
	def get_plagiarism_index( session_key ):
		session = SessionStore( session_key )
		if session.get('plagiarism_index') is not None:
			return session.get('plagiarism_index')
		else:
			return 60
	
class DIP( object ):

	@staticmethod
	def check( doc, session_key ):
		plagiarism_index = DIPSettings.get_plagiarism_index( session_key )
		google = GoogleEngine()
		keys = GoogleSettings.get_keys( session_key )
		google.set_keys( keys )
		
		paragraphs = doc.paragraphs.all()
		for paragraph in paragraphs:
			for sentence in paragraph.sentences.all():
				try:
					json_results = google.query( sentence.text )
				except Exception, e:
					raise e
				if json_results.has_key('items'):
					for item in json_results['items']:
						token = SimilarToken()
						token.url = item['formattedUrl']
						token.text = item['snippet']
						token.sentence = sentence
						DIP.__validate_token( sentence, token, plagiarism_index )
				sentence.search_status = 'COMPLETE'
				sentence.save()
						
		return doc			
		
	@staticmethod
	def __validate_token( sentence, token, plagiarism_index ):
		similarity = DIP.__calculate_token_similarity( sentence.text, token.text )
		
		if similarity >= plagiarism_index:
			token.similarity = similarity
			token.save()

	@staticmethod
	def __calculate_token_similarity( sentence, token ):
		
		token = TextProcessor.remove_stop_words( token )
		token = TextProcessor.remove_special_chars( token )
		similar_set = set( token.split() )
		sentence_set = set( sentence.split() )
		len_sentence_words = len( sentence_set )
		len_equal_words = len( similar_set & sentence_set )
		similarity = len_equal_words*100 / len_sentence_words
		return similarity
