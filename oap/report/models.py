# -*- coding: utf-8 -*-
from django.db import models
from django import http
from django.template.loader import get_template
from django.template import Context
import ho.pisa as pisa
import cStringIO as StringIO
import cgi, os
import logging
from django.conf import settings

def fetch_resources(uri, rel):
	"""
	Callback to allow xhtml2pdf/reportlab to retrieve Images,Stylesheets, etc.
	`uri` is the href attribute from the html link element.
	`rel` gives a relative path, but it's not used here.

	"""
	
	path = os.path.join(settings.STATIC_ROOT, uri.replace(settings.STATIC_URL, ""))
	
	if not os.path.isfile(path):
		raise UnsupportedMediaPathException('media urls must start with %s or %s' % (settings.MEDIA_ROOT, settings.STATIC_ROOT))

	
	return path

class PisaNullHandler(logging.Handler):
    def emit(self, record):
        pass

class PdfReport( models.Model ):

	class Meta:
		managed = False

	def to_pdf( self, template_src, context_dict, filename ):
		logging.getLogger("ho.pisa").addHandler(PisaNullHandler())
		template = get_template(template_src)
		context = Context(context_dict)
		html  = template.render(context)
		result = StringIO.StringIO()
		pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources)
		if not pdf.err:
			response = http.HttpResponse(mimetype='application/pdf')
			response['Content-Disposition'] = 'attachment; filename=%s' % filename
			response.write(result.getvalue())
			return response
		return http.HttpResponse('Problema ao gerar PDF: %s' % cgi.escape(html))
