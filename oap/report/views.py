#-*-coding:utf-8-*-
from django.shortcuts import render_to_response, get_object_or_404, Http404
from django.views.decorators.csrf import csrf_exempt
from .models import *
@csrf_exempt
def default( request ):
	if request.session.get('doc') is not None:
		doc = request.session.get('doc')
		doc_name = request.session.get('doc_name') 
		report = PdfReport()
		return report.to_pdf('report/default.html', locals(), 'relatorio_similaridade.pdf')
	else:
		raise Http404()