#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response, HttpResponse, get_object_or_404, Http404
from django.http import HttpResponseBadRequest 
from django.utils import simplejson
from django.template import RequestContext
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse 
from django.core.files.storage import default_storage 
from django.core.files.uploadedfile import UploadedFile
from django.views.decorators.csrf import csrf_exempt 

import re
from urllib2 import urlopen, quote

from oap.plagiarismchecker.models import *
from oap.plagiarismchecker.forms import *

from oap.searcher.models import *
from oap.searcher.forms import *

from oap.document.models import *
from oap.document.forms import *


def index(request):
	has_keys = GoogleSettings.has_keys( request.session.session_key )
	keys = GoogleSettings.get_keys( request.session.session_key )
	return render_to_response('index.html', locals(), context_instance=RequestContext(request))

def faq(request):
	return render_to_response('faq.html', locals(), context_instance=RequestContext(request))

def contact( request ):
	if request.method == 'POST':
		pass
	else:
		raise Http404()

def results( request ):
	doc = request.session.get('doc')
	return render_to_response('results.html', locals(), context_instance=RequestContext(request))

@csrf_exempt 
def upload( request ): 
	if request.method == 'POST': 
		if request.FILES == None: 
			return HttpResponseBadRequest( 'Must have files attached!' )
		form = DocumentForm( request.POST , request.FILES )
		result = [] 
		if form.is_valid():
			path = form.save()
			request.session['doc'] = path
			request.session['doc_name'] = request.FILES['document'].name 
			result.append(
					{
						"action" : 'upload', 
						"status" : 'ok',
						"filename" : request.FILES['document'].name,
					}
				)
		else:
			result.append(
					{
						'status' : 'error',
						'errors': form.errors,	
					}
				)
		 
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()

@csrf_exempt 
def text_extract( request ):
	if request.method == 'POST':
		path = request.session.get('doc')
		doc = open(path, 'rw')
		doc_as_text = DocumentConverter.doc_to_text( doc )
		doc.close()
		request.session['doc'] = doc_as_text
		result = []
		result.append(
						{
							"action" : 'text_extract', 
							"status" : 'ok',
						}
					)
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()


@csrf_exempt 
def pre_process( request ):
	if request.method == 'POST':
		
		doc_as_text = request.session['doc']
		doc = Document.objects.create_doc( doc_as_text )
		request.session['doc'] = doc
		doc.tokenizer_paragraphs()
		result = []
		result.append(
						{
							"action" : 'pre_process', 
							"status" : 'ok',
						}
					)
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()

@csrf_exempt 
def similarity_search( request ):
	if request.method == 'POST':
		result = []
		try:
			doc = request.session.get('doc')
			doc = DIP.check( doc, request.session.session_key )
			result.append(
							{
								"status" : 'ok',
							}
						)
		except Exception, e:
			raise e
			result.append(
						{
							"status" : 'error', 
							"errors" : { 0:'Falha ao executar a busca no google.',},
						}
					)

		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json')
	else:
		raise Http404()


@csrf_exempt
def status_pre_process( request ):	
	if request.method == 'POST':
		progress = 0
		doc = request.session.get('doc')
		if doc is object:
			total_paragraphs = doc.paragraphs.count()
			completed_paragraphs = Paragraphs.objects.filter( extract_status="COMPLETE", document__pk=doc.pk ).count()
			if total_paragraphs > 0:
				progress = ( completed_paragraphs * 100 )/total_paragraphs
		result = []
		result.append(
						{
							"progress" : progress, 
						}
					)
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()

@csrf_exempt
def status_similarity_search( request ):	
	if request.method == 'POST':
		progress = 0
		doc = request.session.get('doc')
		if doc is object:
			total_sentences = Sentences.objects.filter( paragraph__document=doc ).count()
			completed_sentences = Sentences.objects.filter( paragraph__document=doc, search_status='COMPLETE' ).count()
			if total_paragraphs > 0:
				progress = ( completed_sentences * 100 )/total_sentences

		result = []
		result.append(
						{
							"progress" : progress, 
						}
					)
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()

def settings( request ):
	plagiarism_index = DIPSettings.get_plagiarism_index( request.session.session_key )
	dip_form = SettingsDipForm( initial={'plagiarism_index': plagiarism_index} ) 
	google_form = SettingsGoogleForm()
	processor_form = SettingsTextPreProcessorForm()
	edit_processor_form = EditSettingsTextPreProcessorForm()
	return render_to_response('settings.html', locals(), context_instance=RequestContext(request))

