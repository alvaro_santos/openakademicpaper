from django.conf.urls import patterns, include, url
from django.conf import settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^static/(.*)', 'django.views.static.serve', { 'document_root': settings.STATIC_ROOT } ),
    url(r'^$', 'oap.views.index', name='index'),
    url(r'^faq$', 'oap.views.faq', name='faq'),
    url(r'^upload$', 'oap.views.upload', name="upload"),
    url(r'^report-default$', 'oap.report.views.default', name="reportdefault"),
    url(r'^text-extract$', 'oap.views.text_extract', name="textextract"),
    url(r'^pre-process$', 'oap.views.pre_process', name="preprocess"),
    url(r'^similarity-search$', 'oap.views.similarity_search', name="similaritysearch"),
    url(r'^resultado$', 'oap.views.results', name="results"),
    url(r'^status-pre-process$', 'oap.views.status_pre_process', name="statuspreprocess"),
    url(r'^status-similarity-search$', 'oap.views.status_similarity_search', name="statussimilaritysearch"),
    # Settings
    url(r'^settings$', 'oap.views.settings', name="settings"),
    url(r'^settings-dip$', 'oap.plagiarismchecker.views.settings_dip', name="settingsdip"),
    url(r'^settings-default-dip$', 'oap.plagiarismchecker.views.settings_default_dip', name="settingsdefaultdip"),
    url(r'^settings-google$', 'oap.searcher.views.settings_google', name="settingsgoogle"),
    url(r'^settings-default-google$', 'oap.searcher.views.settings_default_google', name="settingsdefaultgoogle"),
    url(r'^settings-pre-processor$', 'oap.document.views.settings_pre_processor', name="settingspreprocessor"),
    url(r'^settings-edit-pre-processor$', 'oap.document.views.settings_edit_pre_processor', name="settingseditpreprocessor"),
    url(r'^settings-get-pre-processor-settings$', 'oap.document.views.get_pre_processor_settings', name="settingsgetpreprocessor"),
    # url(r'^oap/', include('oap.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
