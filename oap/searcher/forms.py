#-*-coding:utf-8-*-
from django import forms
from .models import *

class SettingsGoogleForm( forms.Form ):
	google_key = forms.CharField(max_length=50)
	google_cx = forms.CharField(max_length=50)