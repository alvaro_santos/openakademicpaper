#-*-coding:utf-8-*-
from django.db import models
from urllib2 import urlopen, quote
from django.utils import simplejson
from django.conf import settings
from django.contrib.sessions.backends.db import SessionStore


class GoogleSettings( object ):
	
	@staticmethod
	def has_keys( session_key ):
		has = settings.GOOGLE_KEY is not '' and settings.GOOGLE_CX is not ''
		if not has:
			
			session = SessionStore( session_key )
			return session.get('google_key') is not None and session.get('google_cx') is not None
		else:
			return has

	@staticmethod
	def get_keys( session_key ):
		keys = ()
		session = SessionStore( session_key )
		if GoogleSettings.has_keys( session_key ):
			if session.get('google_key') is not None:
				keys = {
					'google_key' : session.get('google_key'),
					'google_cx' : session.get('google_cx'),
				}
			else:
				keys = {
					'google_key' : settings.GOOGLE_KEY,
					'google_cx' : settings.GOOGLE_CX,
				}
		return keys



class GoogleEngine(object):
	google_key = ''
	google_cx = ''

	def set_keys( self, keys ):
		self.google_key = keys['google_key']
		self.google_cx = keys['google_cx']
		

	def query( self, query ):
		query = self.__prepare( query )
		url = 'https://www.googleapis.com/customsearch/v1?key=%s&cx=%s&q=%s&alt=json' % ( self.google_key, self.google_cx, query )
		try:
			response = ( urlopen(url) )
		except Exception, e:
			raise Exception('Erro ao efetuar busca no google.')
			
		json = simplejson.loads( response.read() )
		return json
		
	def __prepare( self, query ):
		query = quote( query.encode('utf-8').replace(' ', '+') )
		return query
