#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response, HttpResponse, get_object_or_404, Http404
from .models import *
from .forms import *

from django.views.decorators.csrf import csrf_exempt
from django.contrib.sessions.backends.db import SessionStore
from django.utils import simplejson

@csrf_exempt 
def settings_google( request ):
	if request.method == 'POST':
		
		google_form = SettingsGoogleForm( request.POST )
		result = []
		if google_form.is_valid():
			
			request.session['google_key'] = google_form.cleaned_data.get('google_key')
			request.session['google_cx'] = google_form.cleaned_data.get('google_cx')
			result.append(
							{
								"status" : 'ok', 
							}
						)
		else:

			result.append(
							{
								"status" : 'error', 
								"errors" : google_form.errors, 
							}
						)
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()

@csrf_exempt 
def settings_default_google( request ):
	if request.method == 'POST':
		result = []
		
		if request.session.get('google_key') is not None:
			del request.session['google_key']
		if request.session.get('google_cx') is not None:
			del request.session['google_cx']

		has_keys = GoogleSettings.has_keys( request.session.session_key )
		if has_keys:
			result.append(
							{
								"status" : 'ok',
							}
						)
		else:
			result.append(
							{
								"status" : 'error',
								"errors" : { '0' : 'Não há chaves de acesso padrão',},
							}
						)
			
		response_data = simplejson.dumps( result ) 
		return HttpResponse(response_data, mimetype='application/json') 
	else:
		raise Http404()