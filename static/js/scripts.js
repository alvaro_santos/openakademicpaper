$(document).ready( function(){
	
	var l = window.location;
	var BASE_URL = l.protocol + "//" + l.host + "/";
	var statusPreProcess;
	var statusSimilaritySearch;

	function fillGeneralProgressBar( progress ){
		$('#general-progress .bar').css('width', progress+'%')
	}
	function resetProcess(){
		$('.bar').css('width', 0 );
		$('#filename').html('');
		$('#action').html('Clique em upload para iniciar.');
		$('#general-progress, #reset').css('display','none');
		$('#upload-input div.info').show();
		$('.alert').remove()
		$('.step-status').removeClass('step-success step-error')
	}
	$('#reset').on('click', 'a', function(){
		resetProcess();
	});
	function fillProgressBar( target, progress ){
		$('#'+target+'-progress .bar').css('width', progress+'%');
		if(progress == 100 ){
			$('#'+target+'-progress').hide(2000);
		}
		switch( target ){
			
			case 'text-extract':
				progress += 100;
				break;
			case 'pre-process':	
				progress += 200;
				break;
			case 'search':	
				progress += 300;
				break;
		}
		fillGeneralProgressBar( progress/4 )
	}
	
	function setStepErrorMessage( target, data ){
		var errors = ''
		$('#reset').css('display','block')
		$.each( data[0]['errors'], function( key, value ){
			errors += value+'<br />'
		});
		var error_div = $('<div class="alert alert-error">'+errors+'</div>');
		$('#general-progress').after( error_div );
		$('#'+target+' .step-status').addClass('step-error')

	}

	function getPreProcessStatus(){
		$.ajax({
			'type': 'post',
			'dataType': 'json',
			'async': false,
			'url': BASE_URL+'status-pre-process',
			success: function( data ){
				fillProgressBar('pre-process', data[0]['progress'])
			} 
		});
	}


	function getSimilaritySearchStatus(){
		$.ajax({
			'type': 'post',
			'dataType': 'json',
			'async': false,
			'url': BASE_URL+'status-similarity-search',
			success: function( data ){
				fillProgressBar('search', data[0]['progress'])
			} 
		});
	}

	function similaritySearch(){
		$.ajax({
			'type': 'post',
			'dataType': 'json',
			'async': false,
			'url': BASE_URL+'similarity-search',
			beforeSend: function(){

				$('#action').html('Realizando busca na internet...')
				statusSimilaritySearch = setInterval( getSimilaritySearchStatus, 1000 )
			},
			success: function( data ){
				if(  data[0]['status'] == 'ok' ){
					$('#search .step-status').addClass('step-success');
					window.location = BASE_URL+'resultado'
				}else{

					setStepErrorMessage( 'search', data );
				}
			},
			error: function( data ){
				setStepErrorMessage( 'search', data );
				clearInterval( statusSimilaritySearch )
			},
			complete: function(){
				clearInterval( statusSimilaritySearch )
			},
		})
	}

	function preProcess(){
		$.ajax({
			'type': 'post',
			'dataType': 'json',
			'async': false,
			'url': BASE_URL+'pre-process',
			beforeSend: function(){
				statusPreProcess = setInterval( getPreProcessStatus, 1000 )
				$('#action').html('Preparando documento para pesquisa...')
			},
			success: function( data ){
				$('#pre-process .step-status').addClass('step-success');
				fillProgressBar('pre-process', 100)
				similaritySearch();
			},
			complete: function( data){
				clearInterval( statusPreProcess )
			}
		})
	}

	function textExtract(){
		$.ajax({
			'type': 'post',
			'dataType': 'json',
			'async': false,
			'url': BASE_URL+'text-extract',
			beforeSend: function(){
				$('#action').html('Extraindo texto...');
				fillProgressBar( 'text-extract', 30 );
			},
			success: function( data ){
				$('#text-extract .step-status').addClass('step-success');
				fillProgressBar( 'text-extract', 100 );
				preProcess();
			},
			error: function( data ){
				alert( data.result )
			},

		})
	};

	$('#fileupload').fileupload({
		dataType: 'json',
		beforeSend: function(e, data){
			$('#upload-input div.info').hide()
			$('#general-progress').css('display','block')
			$('#action').text('Realizando upload...')
		},
		done: function (e, data) {
			if(  data.result[0]['status'] == 'ok' ){
				$('#upload-input .step-status').addClass('step-success');
				$('#upload-progress .bar').hide( 2000 );
				$('#filename').html(data.result[0]['filename'])
				textExtract();
			}else{
				setStepErrorMessage( 'upload-input', data.result );
			}
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			fillProgressBar( 'upload-input', progress )
		},
		error: function(e, data ){
			setStepErrorMessage( 'upload-input', data.result );
		}
	});

	$('#settings').click( function(){
		$.fancybox({
			'type': 'iframe',
			'href': BASE_URL+'settings',
			'width': 600,
			'afterClose':function () {
				
			},
		})
	});

	$('#nav-bar-settings a').click( function(){
		selected = $( this ).data('name');
		$('#content-settings').children('div').addClass('disabled');
		$('#'+selected+'-settings').removeClass('disabled');
	});


	function setMessageSettings( target, data ){
		status = data[0]['status']
		if( status == 'ok'){
			var success_div = $('<div class="alert alert-success">Alteração efetuada com sucesso</div>')
			$('#'+target+'-form').before( success_div )
		}else{
			errors = '';
			$.each( data[0]['errors'], function( key, value ){
				errors += value+'<br />'
			});
			var error_div = $('<div class="alert alert-error">'+errors+'</div>');
			$('#'+target+'-form').before( error_div );
		}
		$('#'+target+'-settings .alert').delay(3000).fadeOut(500, function(){
		   $(this).remove();
		});
	}

	$('#submit-dip').click(function(){
		$.ajax({
			'type': 'post',
			'url': BASE_URL+'settings-dip',
			'data': { 'plagiarism_index': $('#id_plagiarism_index').val() },
			success: function( data ){
				setMessageSettings('dip', data);
			},
		});
	});

	$('#default-dip').click(function(){
		$.ajax({
			'type': 'post',
			'url': BASE_URL+'settings-default-dip',
			success: function( data ){
				setMessageSettings('dip', data);
				$('#id_plagiarism_index').val(60)
			},
		});
	});

	$('#submit-google').click(function(){
		$.ajax({
			'type': 'post',
			'url': BASE_URL+'settings-google',
			'data': { 
				'google_key': $('#id_google_key').val(),
				'google_cx': $('#id_google_cx').val(),
			},
			success: function( data ){
				setMessageSettings('google', data);
				parent.location.reload();
				parent.$.fancybox.close();
			},
		});
	});

	$('#default-google').click(function(){
		$.ajax({
			'type': 'post',
			'url': BASE_URL+'settings-default-google',
			success: function( data ){
				setMessageSettings('google', data);
			},
		});
	});

	$('#submit-pre-processor').click(function(){
		$.ajax({
			'type': 'post',
			'url': BASE_URL+'settings-pre-processor',
			'data': { 
				'language': $('#id_language').val(),
				'stop_words': $('#id_stop_words').val(),
				'special_chars': $('#id_special_chars').val(),
			},
			success: function( data ){
				setMessageSettings('pre-processor', data);
			},
		});
	});

	$('#submit-edit-pre-processor').click(function(){
		$.ajax({
			'type': 'post',
			'url': BASE_URL+'settings-edit-pre-processor',
			'data': $('#edit-pre-processor-form').serialize(),
			success: function( data ){
				setMessageSettings('edit-pre-processor', data);
			},
		});
	});
	$('#edit-pre-processor-form #id_language').change(function(){
		$.ajax({
			'type': 'post',
			'url': BASE_URL+'settings-get-pre-processor-settings',
			'data': $('#edit-pre-processor-form').serialize(),
			success: function( data ){
				$('#edit-pre-processor-form #id_stop_words').val( data[0]['stop_words'] );
				$('#edit-pre-processor-form #id_special_chars').val( data[0]['special_chars'] );
			},
		});
	});
	$('.url i').click(function(){
		content = $( this ).parents('.url').siblings('.content');
		if( content.css('display') == 'block' ){
			content.css('display', 'none');
			$(this).removeClass('icon-minus-sign')
			$(this).addClass('icon-plus-sign')
		}else{
			content.css('display', 'block');
			$(this).removeClass('icon-plus-sign')
			$(this).addClass('icon-minus-sign')
		}  
	});



})